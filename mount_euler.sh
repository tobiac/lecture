#!/bin/bash

if [ $# -eq 0 ]; then
	sshfs tobiac@euler.ethz.ch:/cluster/home/tobiac/redo_HPCSEI/exercises/ex05 eulerMount
	if [ $? -eq 0 ]; then
		cd eulerMount && atom . && cd ..
	else
		echo Couldn\'t mount filesystem
		echo Check you have an active VPN connection to the ETH network
	fi
	exit
else
	sudo umount -v eulerMount
	if [ $? -eq 0 ]; then
		echo Filesystem unmounted
	else
		echo Something went wrong with unmounting the filesystem
	fi
	exit
fi
