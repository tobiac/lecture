// File       : benchmark_1Ddiffusion.cpp
// Description: Benchmark 1D diffusion test
// Copyright 2019 ETH Zurich. All Rights Reserved.
#include <iostream>
#include <vector>
#include <cmath>
#include <chrono> // use std::chrono to benchmark your code
#include <typeinfo>
#include <Eigen/Dense>
#include <fstream>

const static Eigen::IOFormat CSVFormat(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", "\n");

class CacheFlusher
{
public:
    using Size = long long int;
    CacheFlusher(Size size = 1<<26 /* 64 MB */) :
        size(size),
        buf(new volatile unsigned char[size])
    {}

    ~CacheFlusher()
    {
        delete[] buf;
    }

    void flush()
    {
        for (Size i = 0; i < size; ++i)
            buf[i] += i;
    }

private:
    const Size size {0};
    volatile unsigned char *buf;
};

void diffusionKernel1D(double* u, double* uTmp, double factor, int N){
  uTmp[0] = u[0] + factor * (u[N-1] - 2.0*u[0] + u[1]);
  for(int j = 1; j < N-2; ++j){
    uTmp[j] = u[j] + factor * (u[j-1] - 2.0*u[j] + u[j+1]);
  }
  uTmp[N-1] = u[N-1] + factor * (u[N-2] - 2.0*u[N-1] + u[0]);
}

std::tuple<double, int> doMeasurement(int N, CacheFlusher &cacheFlusher, int flush){
  double alpha = 1.0e-4;
  int L = 1000;
  double T = 1.0;
  double dx =  L / double(N-1);
  double tau = dx*dx / (2.0*alpha);
  if(tau >= dx*dx / (2*alpha)) printf("Timestep too big! Constraint not satisfied.\n");
  int timesteps = double(T) / tau;
  double factor = (tau*alpha) / (dx*dx);
  printf("Starting measurements with following parameters:\n");
  printf("alpha: %f, dx: %f, tau: %f, timesteps: %i, factor: %f\n", alpha, dx, tau, timesteps, factor);

  // Create array and initial conditions
  auto initCond = [&L](double x){return sin(2*M_PI*x / double(L));};
  std::vector<double> u(N);
  // Create second array to store temp when applying the diffKernel
  std::vector<double> uTmp(N);
  double* uPtr = u.data();
  double* uTmpPtr = uTmp.data();

  for(int i = 0; i < N; ++i){
    u[i] = initCond(i*dx);
  }


  // Do the time measurements
  double tot_time = 0.0;
  for(int i = 0; i < timesteps; ++i){
    // Cache flushing
    if(flush) cacheFlusher.flush();
    auto tStart = std::chrono::steady_clock::now();

    diffusionKernel1D(uPtr, uTmpPtr, factor, N);

    auto tEnd = std::chrono::steady_clock::now();
    double diff = std::chrono::duration <double, std::milli> (tEnd - tStart).count();
    tot_time += diff;

    // Pointer swap
    std::swap(u, uTmp);
  }
  // Result evaluation
/*
  printf("\nTiming finished.\n");

  printf("tot_time: %f\n", tot_time);
  printf("timesteps: %i\n", timesteps);
*/
  return std::make_tuple(tot_time, timesteps);
}

void writeMatrixToFile(Eigen::MatrixXd &m, const char *filename){
	std::ofstream outFile(filename);
	outFile << m.format(CSVFormat);
	outFile.close();
	std::cout << "\nMatrix written to '" << filename << "'" << std::endl;
}

int main()
{
    // convenient tool to flush the cache
    CacheFlusher cacheFlusher;
    std::vector<int> sizes {1<<17, 1<<19, 1<<20, 1<<21};
    int noMeasurements = sizes.size();
    Eigen::MatrixXd outMatrix(noMeasurements*2, 2);
    for(int k = 0; k < 2; ++k){
      for(int i = 0; i < noMeasurements; ++i){
        int n = sizes[i];
        std::tuple<double, int> results = doMeasurement(n, cacheFlusher, k);
        double tot_time = std::get<0>(results);
        int timesteps = std::get<1>(results);
        outMatrix(i+k*noMeasurements, 0) = n;
        outMatrix(i+k*noMeasurements, 1) = (5*timesteps*n)/tot_time*1e-6;
      }
    }

    std::cout << "  Timesteps\t" << "Performance [GFlop/s]" << std::endl;
    std::cout << "  ===================================" << std::endl;
    std::cout << "\nw\\o cache flushing:" << std::endl;
    std::cout << outMatrix.block(0,0,noMeasurements,2) << std::endl;
    std::cout << "\nw\\ cache flushing:" << std::endl;
    std::cout << outMatrix.block(noMeasurements,0, noMeasurements, 2) << std::endl;

    // write matrix to csv for plotting
    writeMatrixToFile(outMatrix, "measurements.csv");

    return 0;
}
