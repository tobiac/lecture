#pragma once

#include "ArrayOfParticles.h"

#include <functional>
#include <omp.h>

namespace ParallelParticlesIterator
{
ArrayOfParticles initialize_particles_1D(
    size_t n_particles, value_t start_pos_x, value_t end_pos_x,
    const std::function<value_t(value_t)> init_function)
{
    ArrayOfParticles particles(n_particles);
    const value_t dx = (end_pos_x - start_pos_x) / n_particles;
#pragma omp parallel for
    for(size_t i=0; i<n_particles; ++i)
    {
        const value_t x = start_pos_x + dx * (i + 0.5);
        particles.pos_x(i) = x;
        printf("pos_x(%i)=%f\n", i, x);
        particles.pos_y(i) = 0;
        particles.gamma(i) = init_function(x);
    }

    return particles;
}

void reset_velocities(ArrayOfParticles & particles)
{
#pragma omp parallel for
    for (size_t i=0; i<particles.size(); ++i)
    {
        particles.vel_x(i) = 0;
        particles.vel_y(i) = 0;
    }
}

void compute_interaction(const ArrayOfParticles & sources,
                                      ArrayOfParticles & targets)
{
    // TODO
    value_t fac;    // factor
    value_t denom;  // denominator
    value_t nom;    // nominator
    value_t diff_x;   // difference value (x-x_i)
    value_t diff_y;   // difference value (y-y_i)
#pragma omp parallel for private(fac, denom, nom, diff_x, diff_y) collapse(2)
    for(size_t i = 0; i < targets.size(); ++i){
      for(size_t j = 0; j < sources.size(); ++j){
        if(i==j) continue;
        // compute velocity in x direction
        fac = sources.gamma(j) / value_t(2.0*M_PI);
        diff_x = targets.pos_x(i) - sources.pos_x(j);
        diff_y = targets.pos_y(i) - sources.pos_y(j);
        nom = -diff_y;
        denom = diff_x*diff_x + diff_y*diff_y;

        targets.vel_x(i) += (fac * nom) / value_t(denom);

        // compute velocity in y direction
        nom = diff_x;

        targets.vel_y(i) += (fac * nom) / value_t(denom);

      }
    }

}

void advect_euler(ArrayOfParticles & particles, const value_t dt)
{
#pragma omp parallel for
    for (size_t i=0; i<particles.size(); ++i)
    {
        particles.pos_x(i) += particles.vel_x(i) * dt;
        particles.pos_y(i) += particles.vel_y(i) * dt;
    }
}

value_t sum_circulation(const ArrayOfParticles& particles)
{
    value_t total = 0;
#pragma omp parallel for reduction(+ : total)
    for (size_t i=0; i<particles.size(); ++i)
    {
        total += particles.gamma(i);
    }
    return total;
}
}
