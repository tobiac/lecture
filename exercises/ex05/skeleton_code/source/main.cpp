#include <fstream>

#include "Profiler.h"
#include <mpi.h>
#include <chrono> // use std::chrono to benchmark your code
#include <sys/stat.h>

#include "SerialParticlesIterator.h"
using namespace SerialParticlesIterator;

bool fileExists(const std::string& filename)
{
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1)
    {
        return true;
    }
    return false;
}

void writeTiming(int nRanks, double runningTime, std::string name){
  std::ofstream outFile;
  if(fileExists(name))
    outFile.open (name, std::ios::out | std::ios::app);
  else{
    outFile.open(name);
  }
  outFile << nRanks << " " << runningTime << "\n";
}

int main (int argc, char ** argv)
{
    double t1 = MPI_Wtime();
    int mpi_rank=0, mpi_size=1;


    MPI_Init(&argc, &argv);

    Profiler profiler;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    if(!mpi_rank) printf("nProcs = %i\n", mpi_size);

    // Compute no. of particles each rank has to store
    size_t global_n_particles = 3600;
    if (argc > 1) global_n_particles = std::stoul(argv[1]);
    if(mpi_rank == 0){
      if((global_n_particles%mpi_size))
        printf("Total number of particles %i not evenly divisible by number of ranks\n", global_n_particles);
    }

    // Take care of uneven distribution on the last rank (Not needed if above printf wasn't called)
    size_t local_n_particles = global_n_particles/mpi_size;
    size_t remainder = global_n_particles%mpi_size;
    if(mpi_rank == mpi_size-1)  local_n_particles+=remainder;

    if (mpi_rank==0)
    {
        printf("Simulating %lu particles.\n", global_n_particles);
        if (argc < 2) printf("To change N particles run as '%s N'\n", argv[0]);
    }

    const size_t n_particles = local_n_particles;
    const value_t extent_x   = 1.0 / value_t(mpi_size);
    const value_t vol_p = 1.0 / global_n_particles;
    const value_t start_x = - 0.5 + mpi_rank*extent_x;
    const value_t end_x = start_x + extent_x;
/*
    printf("extent_x (%i): %f\n", mpi_rank, extent_x);
    printf("start_x (%i): %f\n", mpi_rank, start_x);
    printf("end_x (%i): %f\n", mpi_rank, end_x);
    printf("n_particles(%i) = %i\n", mpi_rank, n_particles);
*/
    // time integration setup:
    const value_t end_time = 2.5;
    const value_t print_freq = 0.1;
    value_t print_time = 0;
    const value_t dt = 0.0001;

    // initialize particles: position and circulation
    std::function<value_t(value_t)> gamma_init_1D = [&] (const value_t x)
    {
        return vol_p * 4 * x / std::sqrt(1 - 4 * x * x);
    };
    ArrayOfParticles particles = initialize_particles_1D(
        n_particles, start_x, end_x, gamma_init_1D);

    // Array of particles which are being received in each pass from the partner rank
    ArrayOfParticles recvParticles(n_particles);

    value_t time = 0;
    size_t step = 0;
    while (time < end_time)
    {
        // 0. init velocities to zero
        profiler.start("clear");
        reset_velocities(particles);
        profiler.stop("clear");

        // Multi Pass Approach
        value_t* sendbuf_x = particles.pos_x();
        value_t* sendbuf_y = particles.pos_y();
        value_t* recvbuf_x = recvParticles.pos_x();
        value_t* recvbuf_y = recvParticles.pos_y();
        // 1. compute local
        profiler.start("compute");

        // Send requests to all other ranks
        int nRequests = 2*(mpi_size-1);
        MPI_Request sendRequests[nRequests];
        int sendCount = 0;
        for(int i = 0; i < mpi_size; ++i){
          if(i == mpi_rank) continue;

          // initiate the asynchronous sending and receiving
          MPI_Isend(sendbuf_x, n_particles,
                    MPI_DOUBLE, i, 0,
                    MPI_COMM_WORLD, &sendRequests[sendCount++]);
          MPI_Isend(sendbuf_y, n_particles,
                    MPI_DOUBLE, i, 1,
                    MPI_COMM_WORLD, &sendRequests[sendCount++]);
        }

        // Compute self interaction (pass 0)
        compute_interaction(particles, particles);

        int sourceRank;
        bool recvTag;
        MPI_Status status;

        for(int i = 0; i < mpi_size-1; ++i){

            MPI_Probe (MPI_ANY_SOURCE, MPI_ANY_TAG,
                      MPI_COMM_WORLD, &status);
            sourceRank = status.MPI_SOURCE;
            recvTag = status.MPI_TAG;

            // Recv x-/y-coordinates from this specific rank
            MPI_Recv ((!recvTag) ? recvbuf_x : recvbuf_y, n_particles,
                      MPI_DOUBLE, sourceRank, recvTag,
                      MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Recv ((recvTag) ? recvbuf_y : recvbuf_x, n_particles,
                      MPI_DOUBLE, sourceRank, !recvTag,
                      MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            // Compute Interaction
            compute_interaction(recvParticles, particles);

        }

        profiler.stop("compute");

        // 2. with new velocities, advect particles positions:
        profiler.start("advect");
        advect_euler(particles, dt);
        profiler.stop("advect");

        // 3. debug measure: make sure circulation stays constant
        profiler.start("sum gamma");
        value_t total_circulation = sum_circulation(particles);
        profiler.stop("sum gamma");

        if ((time+dt) > print_time)
        {
            print_time += print_freq;

            {   // print to file
                MPI_File file;
                profiler.start("fwrite");
                const std::string config = particles.convert2csv();
                char fname[128]; sprintf(fname, "config_%06lu.csv", step);

                MPI_File_open(
                                MPI_COMM_WORLD,
                                fname,
                                MPI_MODE_CREATE | MPI_MODE_WRONLY,
                                MPI_INFO_NULL,
                                &file
                              );

                MPI_File_write_ordered(
                                        file,
                                        config.data(),
                                        config.size(),
                                        MPI_CHAR,
                                        MPI_STATUS_IGNORE
                                      );

                MPI_File_close(&file);
                profiler.stop("fwrite");
            }

            // Reduce total circulation from each rank to rank 0
            MPI_Reduce((!mpi_rank) ? MPI_IN_PLACE : &total_circulation,
                        &total_circulation, 1, MPI_DOUBLE,
                        MPI_SUM, 0, MPI_COMM_WORLD);

            if (mpi_rank == 0)
            {
                /*
                if(time > 0) profiler.printStatAndReset();
                printf("Iteration %lu - time %f - Total Circulation %f\n",
                       step, time, total_circulation);
                */
            }
        }

        // advance time counters:
        time += dt;
        step ++;
    }

    MPI_Finalize();

    if(mpi_rank == 0){
      double t2 = MPI_Wtime();
      writeTiming(mpi_size, t2-t1, "mpiStrong.dat");
    }

	return 0;
}
