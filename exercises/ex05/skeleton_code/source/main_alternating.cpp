#include <fstream>

#include "Profiler.h"
#include <mpi.h>

#include "SerialParticlesIterator.h"
using namespace SerialParticlesIterator;

void sendRecvOperation(int mpi_rank, int partnerRank, value_t* sendbuf, int sendrecvcount, value_t* recvbuf){
  MPI_Sendrecv (sendbuf, sendrecvcount, MPI_DOUBLE, partnerRank, 42,
                recvbuf, sendrecvcount, MPI_DOUBLE, partnerRank, 42,
                MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  MPI_Barrier(MPI_COMM_WORLD);
}

int main (int argc, char ** argv)
{
    MPI_Init(&argc, &argv);

    Profiler profiler;
    int mpi_rank=0, mpi_size=1;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    if(!mpi_rank) printf("nProcs = %i\n", mpi_size);

    // Compute no. of particles each rank has to store
    size_t global_n_particles = 3600;
    if (argc > 1) global_n_particles = std::stoul(argv[1]);
    if(mpi_rank == 0){
      if((global_n_particles%mpi_size))
        printf("Total number of particles %i not evenly divisible by number of ranks\n", global_n_particles);
    }
    size_t local_n_particles = global_n_particles/mpi_size;
    size_t remainder = global_n_particles%mpi_size;
    if(mpi_rank == mpi_size-1)  local_n_particles+=remainder;

    if (mpi_rank==0)
    {
        printf("Simulating %lu particles.\n", global_n_particles);
        if (argc < 2) printf("To change N particles run as '%s N'\n", argv[0]);
    }

    const size_t n_particles = local_n_particles;
    const value_t extent_x   = 1.0 / value_t(mpi_size);
    const value_t vol_p = 1.0 / global_n_particles;
    const value_t start_x = - 0.5 + mpi_rank*extent_x;
    const value_t end_x = start_x + extent_x;

    printf("extent_x (%i): %f\n", mpi_rank, extent_x);
    printf("start_x (%i): %f\n", mpi_rank, start_x);
    printf("end_x (%i): %f\n", mpi_rank, end_x);
    printf("n_particles(%i) = %i\n", mpi_rank, n_particles);

    // time integration setup:
    const value_t end_time = 2.5;
    const value_t print_freq = 0.1;
    value_t print_time = 0;
    const value_t dt = 0.0001;

    // initialize particles: position and circulation
    std::function<value_t(value_t)> gamma_init_1D = [&] (const value_t x)
    {
        return vol_p * 4 * x / std::sqrt(1 - 4 * x * x);
    };
    ArrayOfParticles particles = initialize_particles_1D(
        n_particles, start_x, end_x, gamma_init_1D);

    // Array of particles which are being received in each pass from the partner rank
    ArrayOfParticles recvParticles(n_particles);

    value_t time = 0;
    size_t step = 0;
    while (time < end_time)
    {
        // 0. init velocities to zero
        profiler.start("clear");
        reset_velocities(particles);
        profiler.stop("clear");

        // Multi Pass Approach
        int partnerRank;
        value_t* sendbuf_x = particles.pos_x();
        value_t* sendbuf_y = particles.pos_y();
        value_t* recvbuf_x = recvParticles.pos_x();
        value_t* recvbuf_y = recvParticles.pos_y();
        // 1. compute local
        profiler.start("compute");

        // Compute self interaction (pass 0)
        compute_interaction(particles, particles);

        for(int i = 1; i < mpi_size; ++i){
          // Compute partner rank
          partnerRank = (mpi_rank-i+mpi_size)%mpi_size;
          // Exchange the particle positions
          sendRecvOperation(mpi_rank, partnerRank, sendbuf_x, n_particles, recvbuf_x);
          sendRecvOperation(mpi_rank, partnerRank, sendbuf_y, n_particles, recvbuf_y);
          compute_interaction(particles, recvParticles);
        }

        profiler.stop("compute");

        // 2. with new velocities, advect particles positions:
        profiler.start("advect");
        advect_euler(particles, dt);
        profiler.stop("advect");

        // 3. debug measure: make sure circulation stays constant
        profiler.start("sum gamma");
        value_t total_circulation = sum_circulation(particles);
        profiler.stop("sum gamma");

        if ((time+dt) > print_time)
        {
            print_time += print_freq;

            {   // print to file
                MPI_File file;

                profiler.start("fwrite");
                const std::string config = particles.convert2csv();
                //printf("Rank %i: \n %s\n", mpi_rank, config.c_str());
                char fname[128]; sprintf(fname, "config_%06lu.csv", step);
                MPI_File_open(
                                MPI_COMM_WORLD,
                                fname,
                                MPI_MODE_CREATE | MPI_MODE_WRONLY,
                                MPI_INFO_NULL,
                                &file
                              );
                MPI_File_write_ordered(
                                        file,
                                        config.data(),
                                        config.size(),
                                        MPI_CHAR,
                                        MPI_STATUS_IGNORE
                                      );
                MPI_File_close(&file);
                profiler.stop("fwrite");
            }

            // Reduce total circulation from each rank to rank 0
            MPI_Reduce((!mpi_rank)?MPI_IN_PLACE:&total_circulation,
                        &total_circulation, 1, MPI_DOUBLE,
                        MPI_SUM, 0, MPI_COMM_WORLD);

            if (mpi_rank == 0)
            {
                if(time > 0) profiler.printStatAndReset();
                printf("Iteration %lu - time %f - Total Circulation %f\n",
                       step, time, total_circulation);
            }
        }

        // advance time counters:
        time += dt;
        step ++;
    }

    MPI_Finalize();

	return 0;
}
