#include <iostream>
#include <omp.h>

int main(){
	printf("Max threads = %i\n", omp_get_max_threads());	
#pragma omp parallel
{
	for(int i = 0; i < 5; ++i){
		printf("started loop iteration i = %i at rank %i\n", i, omp_get_thread_num());	
//#pragma omp parallel for 
		for(int j = 0; j < 2; ++j){
			printf("In inner loop iteration j = %i at rank %i\n", j, omp_get_thread_num());
			printf("(%i,%i)\n", i, j);
		}
	}
}
	return 0;
}
