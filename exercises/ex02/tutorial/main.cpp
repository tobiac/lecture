#include <stdio.h>
#include <random>
#include <omp.h>

int main() {
  // generator
  std::default_random_engine gen(19);

  // // same as
  // std::default_random_engine gen;
  // gen.seed(19);

  gen.seed(0); // these two may result in the same sequences
  gen.seed(1);

  printf("%d\n", gen()); // raw output from generator

  // distribution
  std::uniform_real_distribution<double> dis(-0.5, 0.5);
  printf("%f.16\n", dis(gen)); // output from desired distribution

  printf("%d\n", omp_get_num_threads()); // always 1 outside parallel region
  printf("%d\n", omp_get_max_threads());   // how many a parallel region would have

  #pragma omp parallel
  {
    // Critical section prevents race condition but serializes the program.
    // Instead, you should use unique instances of default_random_engine
    // on each thread (with arrays or private variables).
    // Comment out and 'make run_race' to see duplicated lines (race condition)
    #pragma omp critical   
    printf("%d\n", gen());
  }
}
