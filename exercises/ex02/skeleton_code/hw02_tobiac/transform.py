import numpy as np
import matplotlib.pyplot as plt

def readData(filename):
    measurements = np.genfromtxt(filename, delimiter = ' ')
    return measurements

def rescaleData(filename):
    data = readData(filename)
    t1 = data[0, 1]
    t2 = data[1, 0]
    if t1 == 1.0 or t2 == 1.0:
        print("\'"+filename+"\'"+" has already been modified or programm didn't run in parallel.")
        return
    data[0, 1] = 1.0
    for i in range(1, int(data.size / 2)):
        data[i, 1] = t1 / data[i, 1]
    arrayFormat = np.asarray(data)
    np.savetxt(filename, data)

def main():
    rescaleData("walkScaling.dat")
    rescaleData("histScaling.dat")

if __name__ == '__main__':
    main()
