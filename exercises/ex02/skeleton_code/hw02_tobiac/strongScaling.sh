#!/bin/bash

numThreads=(1 2 4 8 16 24 32 48 56 64)
#numThreads=(1 2 3 4)

make clean
make main
for n in "${numThreads[@]}"
do
    export OMP_NUM_THREADS=$n
    echo Running with N=$OMP_NUM_THREADS threads...
    make run
done

python transform.py
./plotScaling

#export OMP_PLACES='cores(24)'
#export OMP_PROC_BIND='true'

#bsub -W 01:00 -n 24 -R fullnode -Is bash
