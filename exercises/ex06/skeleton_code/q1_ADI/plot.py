import numpy as np
import matplotlib.pyplot as plt
import os

def readData(filename):
    chargeDist = np.genfromtxt(filename, delimiter=',')
    return chargeDist

def plotRho(path):
    dir, filename = os.path.split(path)
    filebody = os.path.splitext(path)[0]
    data = readData(path)
    plt.imshow(data)
    #plt.colorbar()
    plt.savefig(filebody + '.png')
    #plt.show()

def plotTimesteps():
    data = np.empty([10000,0])
    for filename in os.listdir('data'):
        if filename.endswith(".dat"):
            print(filename)
            newCol = readData(os.path.join('data', filename))
            print(newCol[:,1])
            #data = np.hstack((data, np.atleast_2d(newCol[:,:]).T))
            data = np.hstack((data, newCol))

    print(data[:,1])

    fig, ax = plt.subplots(2,2)
    ax[0,0].semilogx(data[:,0], data[:,1])
    ax[0,1].semilogx(data[:,2], data[:,3])
    ax[1,0].semilogx(data[:,4], data[:,5])
    ax[1,1].semilogx(data[:,6], data[:,7])

    #ax.grid()
    #plt.plot(data[:,0], data[:,1])
    #plt.plot(data[:,2], data[:,3])
    #plt.plot(data[:,4], data[:,5])
    #ax.set_xlim([0,0.8])
    plt.show()

def main():

    for filename in os.listdir('data'):
        if filename.endswith(".csv"):
            plotRho(os.path.join('data', filename))
            continue

    plotTimesteps()


if __name__ == '__main__':
    main()
