#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <omp.h>
#include <string>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>

// sample collection for diagnostics
struct Diagnostics {
    double time;
    double heat;
    Diagnostics(double time, double heat) : time(time), heat(heat) {}
};

class Diffusion2D
{
public:
    Diffusion2D(const double D, const double L, const int N, const double dt)
        : D_(D), L_(L), N_(N), dt_(dt)
    {
        // Real space grid spacing.
        dr_ = L_ / (N_ - 1);

        // Actual dimension of a row (+2 for the ghost cells).
        real_N_ = N_ + 2;

        // Total number of cells.
        Ntot_ = (N_ + 2) * (N_ + 2);

        phi_.resize(Ntot_, 0.);
        rhs_.resize(Ntot_, 0.);

        // Initialize field on grid
        initialize_phi();

        // Common pre-factor
        R_ = D * dt / (2. * dr_ * dr_);

        // TODO:
        // Initialize diagonals of the coefficient
        // matrix A, where Ax=b is the corresponding
        // system to be solved
        double R_diag = (1 + 2*R_);
        a_.resize(real_N_-1, -R_);
        b_.resize(real_N_, R_diag);
        c_.resize(real_N_-1, -R_);

        b_[0] = 1.0;
        b_[real_N_-1] = 1.0;
        a_[real_N_-2] = 0.0;
        c_[0] = 0.0;

        printf("before loop\n");
        for (int i = 1; i < real_N_; ++i){
          // since we always use the same matrix we can directly eliminate
          // the lower diagonal 'a' by transforming also 'b' and 'c'
          b_[i] -= c_[i-1]*(a_[i-1]/b_[i-1]);
        }

    }

    void advance()
    {
        // TODO:
        // Implement the ADI scheme for diffusion
        // and parallelize with OpenMP
        bool step = 0;

        // ADI Step 1: Update columns at half timestep
        // Solve implicit system with Thomas algorithm

        // update each column of phi_ with thomas algorithm
        compute_rhs(step);
#pragma omp parallel for
        for(int j = 1; j < real_N_-1; ++j){
          solve_thomas(step, j);
        }

        // ADI: Step 2: Update rows at full timestep
        step = 1;

        // update each row of phi_ with thomas algorithm
        compute_rhs(step);
#pragma omp parallel for
        for(int i = 1; i < real_N_-1; ++i){
          solve_thomas(step, i);
        }
        // Solve implicit system with Thomas algorithm
    }

    void compute_diagnostics(const double t)
    {
        double heat = 0.0;
        // Compute the integral of phi_ in the computational domain
#pragma omp parallel for collapse(2) reduction(+:heat)
        for (int i = 1; i < real_N_ - 1; ++i){     // rows
            for (int j = 1; j < real_N_ - 1; ++j){ // columns
              heat += dr_ * dr_ * phi_[locGlob(0,i,j)];
            }
        }


#ifndef NDEBUG
        std::cout << "t = " << t << " heat = " << heat << '\n';
#endif
        diag_.push_back(Diagnostics(t, heat));
    }

    void write_diagnostics(const std::string &filename) const
    {
        std::ofstream out_file(filename, std::ios::out);
        for (const Diagnostics &d : diag_)
            out_file << d.time << '\t' << d.heat << '\n';
        out_file.close();
    }

    void printPhi(){
      for (int i = 1; i < real_N_ - 1; ++i){     // rows
          for (int j = 1; j < real_N_ - 1; ++j){ // columns
            printf("locGlob(0,%i,%i) = %i\n", i,j,locGlob(0,i,j));
            //printf("%f  ", phi_[locGlob(0, i, j)]);
          }
          printf("\n");
      }
    }

    void savePhi(int iteration){
    	std::ostringstream oss;
    	oss << "data/phi_" << iteration << ".csv";
    	std::string filename = oss.str();
    	std::ofstream outStream;
    	outStream.open(filename);
    	for(int i = 0; i < real_N_; ++i){
    		for(int j = 0; j < real_N_; ++j){
    			outStream << phi_[i*real_N_ + j] << ',';
    		}
    		outStream << '\n';
    	}
    	printf("File saved to %s.\n", filename.c_str());
    }

private:
    int locGlob(bool dir, int i, int k){
        return (!dir) ? (i*real_N_+k) : (k*real_N_+i);
    }

    void initialize_phi()
    {
        // Initialize phi(x, y, t=0)
        double bound = 0.25 * L_;
        printf("Bound = %f\n", bound);
        // TODO:
        // Initialize field phi based on the
        // prescribed initial conditions
        // and parallelize with OpenMP
#pragma omp parallel for collapse(2)
        for (int i = 1; i < real_N_ - 1; ++i){     // rows
            for (int j = 1; j < real_N_ - 1; ++j){ // columns
              int idx = i*real_N_ + j;
              if(std::abs(L_/2 - i*dr_) < bound && std::abs(-L_/2 + j*dr_) < bound){
                phi_[idx] = 1.0;
              } else {
                phi_[idx] = 0.0;
              }
            }
        }
    }

    void compute_rhs(bool dir){
      int firstIdx;
      int secondIdx;
      int thirdIdx;

      // compute all right hand sides
#pragma omp parallel for collapse(2)
      for(int i = 1; i < real_N_-1; ++i){
        for(int j = 1; j < real_N_-1; ++j){
          firstIdx = locGlob(dir, i, j-1);
          secondIdx = locGlob(dir, i, j);
          thirdIdx = locGlob(dir, i, j+1);

          rhs_[secondIdx] = R_*phi_[firstIdx] - 2*R_*phi_[secondIdx] + R_*phi_[thirdIdx];
        }
      }
    }

    void thomas_transform_rhs(bool dir, int k){
      for(int i = 1; i < real_N_; ++i){
        rhs_[locGlob(dir,i,k)] -= rhs_[locGlob(dir,i-1,k)] * (a_[i-1]/b_[i-1]);
      }
    }

    void solve_thomas(bool dir, int k){
      thomas_transform_rhs(dir, k);
      phi_[locGlob(dir,real_N_-1,k)] = rhs_[locGlob(dir,real_N_-1,k)] / b_[real_N_-1];
      for(int i = real_N_-2; i >= 0; --i){
          phi_[locGlob(dir,i,k)] = (rhs_[locGlob(dir,i,k)] - c_[i]*phi_[locGlob(dir,i+1,k)]) / b_[i];
      }
    }

    double D_, L_;
    int N_, Ntot_, real_N_;
    double dr_, dt_;
    double R_;
    std::vector<double> phi_, rhs_;
    std::vector<Diagnostics> diag_;
    std::vector<double> a_, b_, c_;
};

// No additional code required from this point on
int main(int argc, char *argv[])
{
    if (argc < 5) {
        std::cerr << "Usage: " << argv[0] << " D L N dt\n";
        return 1;
    }

#pragma omp parallel
#pragma omp master
        std::cout << "Running with " << omp_get_num_threads() << " threads\n";

    const double D = std::stod(argv[1]);  // diffusion constant
    const double L = std::stod(argv[2]);  // domain side (length)
    const int N = std::stoul(argv[3]);    // number of grid points per dimension
    const double dt = std::stod(argv[4]); // timestep

    Diffusion2D system(D, L, N, dt);

    const auto start = std::chrono::system_clock::now();
    for (int step = 0; step < 10000; ++step) {
        if(!(step%3) && step < 13)  system.savePhi(step);
        if(step == 9999)  system.savePhi(step);
        system.advance();
        system.compute_diagnostics(dt * step);

    }
    const auto end = std::chrono::system_clock::now();
    auto elapsed =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

    std::cout << "Timing: "
              << "N=" << N << " elapsed=" << elapsed.count() << " ms" << '\n';

    //system.printPhi();

    std::ostringstream oss;
    oss << "data/diagnostics_" << dt << ".dat";
    std::string filename = oss.str();

    system.write_diagnostics(filename.c_str());

    return 0;
}
