#!/usr/bin/env bash

cat rank0 > tmp
rm -f rank0
for f in rank* ; do
    cat $f >> tmp
    rm -f $f
done

sort -k1n < tmp > output
rm -f tmp

exit 0
