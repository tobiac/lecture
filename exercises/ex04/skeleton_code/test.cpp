#include <ctime>
#include <fstream>
#include <mpi.h>
int func(){
    MPI_Barrier(MPI_COMM_WORLD);
    return 0;
}
int main(int argc, char* argv[])
{

    int rank, procs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &procs);

    // get a reference
    func();
    const size_t t0 = clock();

    std::ofstream out("rank" + std::to_string(rank));
    out << "# " << t0 << "\n";
    for (int step = 0; step < 100; ++step) {
        func();
        const size_t t1 = clock();
        out << t1 - t0 << " step: " << step << " rank: " << rank << "\n";
    }
    func();
    const size_t t1 = clock();
    out << "# " << t1 << " " << t1 - t0 << "\n";
    out.close();
    // TODO: Shutdown the MPI environment
    // *** start MPI part ***
    MPI_Finalize();
    // *** end MPI part ***
    return 0;
}
