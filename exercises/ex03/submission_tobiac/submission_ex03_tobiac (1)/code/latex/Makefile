# File       : Makefile
# Created    : Sun Oct 08 2017 07:49:08 PM (+0200)
# Description: Generic Makefile for (multi-file) LaTeX documents.  Assumes you
#              are using BibTeX for your references.
#              Another alternative: http://latex-mk.sourceforge.net/
# Copyright 2017 ETH Zurich. All Rights Reserved.

# change this variable to the name of your main file
MAINFILE = report

# Tools
LATEX    = latex
PDFLATEX = pdflatex -shell-escape
PSTOPDF  = ps2pdf
BIBTEX   = bibtex
MKINDEX  = makeindex
DVIPS    = dvips
DVIPDF   = dvipdf
PDFTOEPS = pdftops -eps
PDFTOPS  = pdf2ps
EPSTOPDF = epstopdf
FIGTODEV = fig2dev
MPOST    = mpost

# default document compiler
CC = $(PDFLATEX)

# find all .tex files down the directory tree (for multi-file projects)
TEXFILES = $(shell find . -type f -iname '*.tex')

.PHONY: fast index bib resolved resolved-more clean clean-hysterical

# default target -- make fast for quick check
fast: $(TEXFILES)
	$(CC) $(MAINFILE).tex

index: $(TEXFILES) fast
	$(MKINDEX) $(MAINFILE).nlo -s nomencl.ist -o $(MAINFILE).nls

bib: $(TEXFILES) fast
	$(BIBTEX) $(MAINFILE).aux

resolved: bib
	$(CC) $(MAINFILE).tex
	$(CC) $(MAINFILE).tex

resolved-more: bib index
	$(CC) $(MAINFILE).tex
	$(CC) $(MAINFILE).tex

clean:
	rm -f *.aux *~
	rm -f $(MAINFILE).dvi $(MAINFILE).idx $(MAINFILE).ind $(MAINFILE).ilg \
		$(MAINFILE).out $(MAINFILE).toc $(MAINFILE).pdf $(MAINFILE).log \
		$(MAINFILE).ps $(MAINFILE).thm $(MAINFILE).nlo $(MAINFILE).bbl \
		$(MAINFILE).blg $(MAINFILE).lot $(MAINFILE).lof

# clean more stuff down the directory tree
clean-hysterical: clean
	find . -type f -name '*~' -exec rm -f {} \;
