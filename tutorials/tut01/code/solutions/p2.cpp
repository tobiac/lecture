#include <stdio.h>
#include <stdlib.h>
#include <vector>

int main(int argc, char** argv)
{
  size_t N = 8;

  // Allocating a square matrix
  std::vector<double> myMatrix(N*N);
  std::vector<double> rowSums(N);
  std::vector<double> colSums(N);

  // Initializing the matrix's vales
  for (size_t i = 0; i < N; i++)
   for (size_t j = 0; j < N; j++)
    myMatrix[i*N + j] = 2*i+j;

  // Initializing sums vectors
  for (size_t i = 0; i < N; i++)
  {
   rowSums[i] = 0.0;
   colSums[i] = 0.0;
  }

  // Calculating row and column sums.
  for (size_t i = 0; i < N; i++)
   for (size_t j = 0; j < N; j++)
   {
    rowSums[i] += myMatrix[i*N + j];
    colSums[i] += myMatrix[j*N + i];
   }

  // Printing Sums:
  for (size_t i = 0; i < N; i++)
  {
   printf("Row %lu - Sum: %f\n", i, rowSums[i]);
   printf("Col %lu - Sum: %f\n", i, colSums[i]);
  }

  printf("\nFinished. \n");
  return 0;
}

 
