#include <stdio.h>
#include <stdlib.h>
#include <vector>

int main(int argc, char** argv)
{
  std::vector<double> vA = { 1.4, 6.3, 9.1, 5.7, 0.3, 0.6 };
  std::vector<double> vB = { 9.1, 0.0, 8.7, 6.1, 6.2, 4.8 };
  std::vector<double> result;

  result.resize(vA.size());
  for (size_t i = 0; i < vA.size(); i++)
   result[i] = vA[i] + vB[i];

  printf("Result: ");
  for (size_t i = 0; i < result.size(); i++) printf("%f ", result[i]);

  printf("\nFinished. \n");
  return 0;
}

 
