#include <stdio.h>
#include <stdlib.h>
#include <vector>

int main(int argc, char** argv)
{
  size_t N = 8;

  // Allocating a square matrix
  std::vector<double> myMatrix(N*N);
  std::vector<double> myVector(N);
  std::vector<double> result(N);

  // Initializing the matrix's vales
  for (size_t i = 0; i < N; i++)
   for (size_t j = 0; j < N; j++)
    myMatrix[i*N + j] = 2*i+j;

  // Initializing the vector's vales
  for (size_t i = 0; i < N; i++)
   myVector[i] = 2*i;

  // Initializing result vector
  for (size_t i = 0; i < N; i++) result[i] = 0.0;

  // Your solution for A*x goes here.

  // Printing result
  printf("A*x = ");
  for (size_t i = 0; i < result.size(); i++) printf("%f ", result[i]);

  printf("\nFinished. \n");
  return 0;
}

 
