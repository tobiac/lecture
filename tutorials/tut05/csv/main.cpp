#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <random>
#include <mpi.h>

struct Particle {
  double x, y, z;
  double r;
};

std::vector<Particle> GetParticles(int rank, int commsize, int N) {
  std::default_random_engine g;
  std::uniform_real_distribution<double> u(-0.5, 0.5);

  std::vector<Particle> pp;
  for (int i = 0; i < N; ++i) {
    double x = (u(g) + rank + 0.5) / commsize - 0.5;
    double y = u(g) * (1 - x * x);
    double z = u(g) * (1 - x * x);
    double r = std::sqrt(x * x + y * y + z * z);
    pp.push_back({x, y, z, r});
  }
  return pp;
}

std::string GetCsvHeader() {
  return "x,y,z,r";
}

std::string GetCsvParticles(const std::vector<Particle>& pp) {
  std::stringstream s;
  for (auto& p : pp) {
    s << p.x << "," << p.y << "," << p.z << "," << p.r << "\n";
  }
  return s.str();
}

void WriteManyFiles(const std::vector<Particle>& pp, int rank) {
  std::string path = "part_" + std::to_string(rank) + ".csv";
  std::cout << path << std::endl;

  std::ofstream fout(path);
  fout << GetCsvHeader() << std::endl;
  fout << GetCsvParticles(pp);
}

void WriteRoot(const std::vector<Particle>& pp,
               MPI_Comm comm, int rank, int commsize) {
  std::string csv = GetCsvParticles(pp);
  int cnt = csv.size();

  if (rank == 0) {
    std::vector<int> cnts(commsize);
    MPI_Gather(&cnt, 1, MPI_INT,
               cnts.data(), 1, MPI_INT, 0, comm);

    std::vector<int> displs(commsize + 1);
    displs[0] = 0;
    for (int i = 0; i < commsize; ++i) {
      displs[i + 1] = displs[i] + cnts[i];
    }

    std::vector<char> buf(displs[commsize]);
    MPI_Gatherv(csv.data(), cnt, MPI_CHAR,
                buf.data(), cnts.data(), displs.data(), MPI_CHAR, 0, comm);

    const std::string path = "part.csv";
    std::cout << path << std::endl;
    std::ofstream fout(path);
    fout << GetCsvHeader() << std::endl;
    for (auto c : buf) {
      fout << c;
    }
  } else {
    MPI_Gather(&cnt, 1, MPI_INT,
               nullptr, 0, MPI_INT, 0, comm);
    MPI_Gatherv(csv.data(), cnt, MPI_CHAR,
                nullptr, nullptr, nullptr, MPI_CHAR, 0, comm);
  }
}

void WriteMpi(const std::vector<Particle>& pp, MPI_Comm comm, int rank) {
  const std::string path = "partmpi.csv";
  if (rank == 0) {
    std::cout << path << std::endl;
  }
  MPI_File fout;
  MPI_File_open(comm, path.data(),
                MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fout);

  if (rank == 0) {
    auto h = GetCsvHeader() + '\n';
    MPI_File_write_shared(fout, h.data(), h.size(),
                          MPI_CHAR, MPI_STATUS_IGNORE);
  }

  std::string csv = GetCsvParticles(pp);
  MPI_File_write_ordered(fout, csv.data(), csv.size(),
                         MPI_CHAR, MPI_STATUSES_IGNORE);
  MPI_File_close(&fout);
}


int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);
  MPI_Comm comm = MPI_COMM_WORLD;

  int size, rank;
  MPI_Comm_size(comm, &size);
  MPI_Comm_rank(comm, &rank);

  const int N = 10000;
  auto pp = GetParticles(rank, size, N);

  WriteManyFiles(pp, rank);
  WriteRoot(pp, comm, rank, size);
  WriteMpi(pp, comm, rank);

  MPI_Finalize();
}
