from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

ss = GetSources()
for sn in ss:
  s = ss[sn]
  ExtendFileSeries(s)
  ReloadFiles(s)

