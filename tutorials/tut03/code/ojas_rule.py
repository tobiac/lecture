import numpy as np
import matplotlib
# LOAD THIS IF YOU RUN ON A CLUSTER
# matplotlib.use("Agg")
import matplotlib.pyplot as plt
import os
from sklearn.decomposition import PCA
from itertools import cycle
import scipy

import math, matplotlib.patches as patches
from matplotlib import animation


method = "OJA"

dataset = "2D"

data = np.loadtxt("./data/{:}_dataset.txt".format(dataset), delimiter=',')

N, D = np.shape(data)

K = 2

data_mean = np.mean(data,axis=0)
data_std = np.std(data,axis=0)
data_centered = data - data_mean

data_cycl = cycle(data)

beta=1e-3
iter_max = 100000
plt_every = 1000
normalize_every = 100

# W=np.eye(K,D)
# print(W)
W=np.random.rand(K,D)
# W = scipy.linalg.orth(W)

def getGradient(x, K, D):
  # print(np.shape(x))
  x = np.reshape(x, (D,1))
  gradient=np.zeros((K,D))
  for k in range(K):
    w_k = np.reshape(W[k], (D,1))
    y_k = w_k.T @ x
    # print(np.shape(y_k))
    sum_ = np.zeros((D, 1))
    for j in range(k):
      w_j = np.reshape(W[j], (D,1))
      y_j = w_j.T @ x
      sum_ += w_j*y_j
    dw = y_k * (x - sum_)
    # dw = y_k * (x - W[k]*y_k)
    gradient[k,:]=dw[:,0]
  return gradient

# gradient = getGradient(next(data_cycl), K, D)


# PLOTTING
# Create figure
fig = plt.figure()    
ax = fig.gca()
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_ylim(-8,8)
ax.set_xlim(-8,8)
plt.gca().set_aspect('equal', adjustable='box')
plt.plot(data[:,0], data[:,1], "x", color="forestgreen", zorder=0)
# plt.show()


for iter_ in range(iter_max):
  print("Iter {:}/{:}, {:}%".format(iter_, iter_max, iter_/iter_max*100.0))
  x = next(data_cycl)
  # print(x)
  gradient = getGradient(x, K, D)
  W+=beta*gradient
  for k in range(K):
    print("NORMS: |W|= {:}".format(np.linalg.norm(W[k])))


  if iter_ % normalize_every ==0:
    for k in range(K):
      W[k] = W[k] / np.linalg.norm(W[k])


  if iter_ % plt_every ==0:
    plt.clf()
    plt.plot(data[:,0], data[:,1], "x", color="forestgreen", zorder=0)
    for k in range(K):
      plt.arrow(0, 0, W[k, 0], W[k, 1], linewidth=3, head_width=0.15, head_length=0.2, alpha=0.75, zorder=1) #, color="red"
    plt.pause(0.05)


plt.close()

# NORMALIZE
for k in range(K):
  W[k] = W[k] / np.linalg.norm(W[k])
print(W)

if k>1:
  print(W[0].T @ W[1])


Y = W @ data.T
print(np.shape(Y))
# [0.44051275 3.81556062]
#K=1 gives 3.81183218
#K=2 gives [3.81183218 0.44013902]
eig_pred = np.var(Y, axis=1)
print(np.shape(eig_pred))
print(eig_pred)


pca_components=W

np.savetxt("./results/{:}{:}_components.txt".format(dataset, method), pca_components, delimiter=',')
np.savetxt("./results/{:}{:}_mean.txt".format(dataset, method), data_mean, delimiter=',')
np.savetxt("./results/{:}{:}_std.txt".format(dataset, method), data_std, delimiter=',')
np.savetxt("./results/{:}{:}_eig.txt".format(dataset, method), eig_pred, delimiter=',')





