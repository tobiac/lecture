#pragma once

#include "definitions.h"
#include "particles.h"

#include <cmath>
#include <algorithm>

struct CellListsView
{
    Domain domain;
    real rc;
    int2 nCells;
    int totNCells;
    real2 h;
    
    const int *cellStarts {nullptr};
    const int *cellCounts {nullptr};

    int clampX(int ix) const {return std::min(nCells.x-1, std::max(0, ix));}
    int clampY(int iy) const {return std::min(nCells.y-1, std::max(0, iy));}
    
    int2 positionToCidNotClamped(real2 r) const
    {
        r.x += 0.5_r * domain.localSize.x;
        r.y += 0.5_r * domain.localSize.y;
        
        return {(int) std::floor(r.x / h.x),
                (int) std::floor(r.y / h.y)};
    }
    
    int2 positionToCidClamped(real2 r) const
    {
        const int2 cid = positionToCidNotClamped(r); 
        
        return {clampX(cid.x),
                clampY(cid.y)};
    }

    int toLinearCid(int2 cid) const
    {
        return cid.x + nCells.x * cid.y;
    }

    int2 toCid(int i) const
    {
        return {i % nCells.x,
                i / nCells.x};
    }

protected:
    CellListsView(const Domain& domain, real rc);
};

class CellLists : public CellListsView
{
public:
    CellLists(const Domain& domain, real rc);

    void build(std::vector<Particle>& particles);
    CellListsView getView() const;
    
private:
    std::vector<Particle> helper;
    std::vector<int> starts, counts;
};
