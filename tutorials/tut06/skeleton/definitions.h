#pragma once

#include <cstdlib>
#include <mpi.h>

using real = double;


constexpr real operator "" _r (long double x)
{
    return static_cast<real>(x);
}

struct real2
{
    real x, y;
};

struct int2
{
    int x, y;
};

struct Domain
{
    Domain(MPI_Comm comm, real2 localSize, int2 nRanks2D) :
        localSize(localSize)
    {
        int rank;
        MPI_Comm_rank(comm, &rank);
        ranks2D.x = rank % nRanks2D.x;
        ranks2D.y = rank / nRanks2D.x;
    }

    real2 local2global(real2 r) const
    {
        r.x += localSize.x * (ranks2D.x + 0.5_r);
        r.y += localSize.y * (ranks2D.y + 0.5_r);
        return r;
    }
    
    int2 ranks2D;
    real2 localSize;
};


template <typename T> MPI_Datatype inline getMPIFloatType();
template <> MPI_Datatype inline getMPIFloatType<float> () {return MPI_FLOAT;}
template <> MPI_Datatype inline getMPIFloatType<double>() {return MPI_DOUBLE;}
