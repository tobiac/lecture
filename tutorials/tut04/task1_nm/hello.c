#include <stdio.h>

void Hello(void)
{
    printf("Hello World!\n");
}

void Bye(void)
{
    printf("Bye World! (Don't worry, I'll be back)\n");
}
