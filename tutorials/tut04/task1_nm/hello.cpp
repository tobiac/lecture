#include <iostream>

void Hello(void)
{
    std::cout << "Hello World!" << std::endl;
}

void Bye(void)
{
    std::cout << "Bye World! (Don't worry, I'll be back)" << std::endl;
}
