// File       : myapp.cpp
// Created    : Fri Oct 18 2019 04:00:00 PM (+0200)
// Author     : Fabian Wermelinger
// Description: Sample application that links to a library installed with the
//              system package manager (e.g. apt, yum, pacman, etc.).  We will
//              be using BLAS level 1 routines.
// Copyright 2019 ETH Zurich. All Rights Reserved.

// Standard library, the compiler ships with this already so it knows where to
// look for it
#include <iostream>

// This is the header we need for our level 1 routines below.  In order for this
// to work you probably need to install the package that maintains this code.
// Unfortunately this is not uniform across Linux distributions and you often
// end up googeling to find out the name of the package you need.  Here are a
// few examples:
//
// Ubuntu: sudo apt-get install libopenblas-dev libatlas-base-dev
// Arch:   sudo pacman -Sy cblas
//
// If you want to see if the cblas is installed on your system and the linker
// can find it you can use the command
//
// ldconfig -p | grep cblas
#include <cblas.h>

/// @brief Print my 4-element vector
///
/// @param x[4]
void myprint(const double x[4])
{
    std::cout << "[";
    std::cout << x[0] << ",";
    std::cout << x[1] << ",";
    std::cout << x[2] << ",";
    std::cout << x[3] << "]\n";
}

int main(int argc, char *argv[])
{
    double x[4] = {0, 1, 2, 3};
    double y[4];

    myprint(x);

    cblas_dcopy(4, x, 1, y, 1); // BLAS copy x into y
    myprint(y);

    cblas_dscal(4, 2.0, y, 1); // BLAS scale y by factor 2.0
    myprint(y);

    return 0;
}
