// File       : test.h
// Created    : Fri Oct 18 2019 03:15:02 PM (+0200)
// Author     : Fabian Wermelinger
// Description: Our test library header
// Copyright 2019 ETH Zurich. All Rights Reserved.
#ifndef TEST_H_DUGI1AMD
#define TEST_H_DUGI1AMD

namespace test
{
    void print();
} // namespace test

#endif /* TEST_H_DUGI1AMD */
