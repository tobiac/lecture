// File       : libtest.cpp
// Created    : Fri Oct 18 2019 03:12:50 PM (+0200)
// Author     : Fabian Wermelinger
// Description: Implementation of our test library
// Copyright 2019 ETH Zurich. All Rights Reserved.
#include <iostream>

namespace test
{
void print() { std::cout << "Hello from libtest.cpp" << '\n'; }
} // namespace test
