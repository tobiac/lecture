// File       : myapp.cpp
// Created    : Fri Oct 18 2019 03:31:42 PM (+0200)
// Author     : Fabian Wermelinger
// Description: Sample application code that uses our test library
// Copyright 2019 ETH Zurich. All Rights Reserved.

// When you include headers from non-standard locations, the argument to
// #include should be enclosed by "".  Headers in system locations should be
// enclosed by <> instead.  We can just write "test.h" below and the compiler
// will find it because we use the -I option in the Makefile recipe.  Remove it
// and try to compile again.
#include "test.h"

int main(void)
{
    // here we use one function defined in our test library, guarded by the
    // "test" namespace.  Of course you can also define your own types in your
    // library etc.
    test::print();
    return 0;
}
